FROM mcr.microsoft.com/dotnet/sdk:6.0-alpine AS build
WORKDIR /app
COPY ./ ./
RUN dotnet publish WebApiMetrics/WebApiMetrics.csproj \
    -p:PublishSingleFile=true \
    -r=linux-musl-x64 \
    -c=Release \
    --self-contained=true \
    -p:EnableCompressionInSingleFile=true \
    -p:PublishTrimmed=true \
    -o out

FROM mcr.microsoft.com/dotnet/runtime-deps:6.0-alpine3.14-amd64 AS runtime
WORKDIR /app
ENV ASPNETCORE_URLS=http://+:3000
LABEL maintainer="az"
RUN addgroup -g 34000 az
RUN adduser -D -G az --uid 34000 az
# RUN chown -R az:az /app
EXPOSE 3000
USER az
# copy published files in destination folder
COPY --from=build /app/out ./
ENTRYPOINT ["./WebApiMetrics"]

# docker build -t sbraer/webapimetrics .
# docker push sbraer/webapimetrics
# docker tag sbraer/webapimetrics sbraer/webapimetrics:v1.1
# docker push sbraer/webapimetrics:v1.1