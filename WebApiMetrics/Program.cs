using System.Net;
using Prometheus;
using WebApiMetrics;

const string pathMetrics = "/metrics";
using MetricsApp metrics = new(pathMetrics);

var builder = WebApplication.CreateBuilder(args);
var app = builder.Build();
app.UseRouting();
app.UseHttpMetrics();
app.MapMetrics(pattern: pathMetrics);

app.Use(metrics.Use);
app.MapGet("/", () => "Hello, World! v1.0");
app.MapGet("/host", () => $"Host: {Dns.GetHostName()}");
app.MapGet("/health", () => "health check");
app.Run();
