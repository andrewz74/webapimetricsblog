using System.Diagnostics;
using Prometheus;

namespace WebApiMetrics;

public sealed class MetricsApp : IDisposable
{
    private readonly Counter _counter1, _counter2, _counter3, _counter4;
    private readonly string _pathMetrics;
    private readonly Process _proc;

    public MetricsApp(string pathMetrics)
    {
        _pathMetrics = pathMetrics ?? throw new ArgumentException(nameof(pathMetrics));
        _proc = Process.GetCurrentProcess();
        _counter1 = Metrics.CreateCounter(
            "webapimethods_path_counter",
            "requests to webapimethods",
            new CounterConfiguration
            {
                LabelNames = new[] { "method", "endpoint" }
            });

        _counter2 = Metrics.CreateCounter(
            "private_memory_size_64",
            "PrivateMemorySize64");

        _counter3 = Metrics.CreateCounter(
            "gc_get_total_memory",
            "GC.GetTotalMemory");

        _counter4 = Metrics.CreateCounter(
            "system_environment_workingset",
            "System.Environment.WorkingSet");
    }

    public void Dispose()
    {
        _proc.Dispose();
    }

    public Task Use(HttpContext context, Func<Task> next)
    {
        var path = context.Request.Path.Value;
        if (path is not null
            && path != _pathMetrics
            && path != "/health"
            && path != "/favicon.ico")
        {
            _counter1.WithLabels(context.Request.Method, path).Inc();
            _counter2.IncTo(_proc.PrivateMemorySize64);
            _counter3.IncTo(GC.GetTotalMemory(true));
            _counter4.IncTo(Environment.WorkingSet);
        }

        return next.Invoke();
    }
}